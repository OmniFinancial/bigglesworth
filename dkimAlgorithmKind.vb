﻿
' This file is part of Bigglesworth.
'
' Bigglesworth is free software: you can redistribute it and/or modify
' it under the terms of the GNU General Public License as published by
' the Free Software Foundation, either version 3 of the License, or
' (at your option) any later version.
'
' Bigglesworth is distributed in the hope that it will be useful,
' but WITHOUT ANY WARRANTY; without even the implied warranty of
' MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
' GNU General Public License for more details.
'
' You should have received a copy of the GNU General Public License
' along with Bigglesworth.  If not, see <http://www.gnu.org/licenses/>.

''' <summary>
''' Enumeration of the kinds of signature and hashing algorithms
''' that can be used with DKIM.
''' </summary>
''' <remarks></remarks>
Public Enum dkimAlgorithmKind As Integer
    ''' <summary>
    ''' RSA SHA-1 hashing algorithm should be used.
    ''' </summary>
    ''' <remarks></remarks>
    RsaSha1
    ''' <summary>
    ''' RSA SHA-2 (256) hashing algorithm should be used.
    ''' </summary>
    ''' <remarks></remarks>
    RsaSha256
End Enum
