﻿
' This file is part of Bigglesworth.
'
' Bigglesworth is free software: you can redistribute it and/or modify
' it under the terms of the GNU General Public License as published by
' the Free Software Foundation, either version 3 of the License, or
' (at your option) any later version.
'
' Bigglesworth is distributed in the hope that it will be useful,
' but WITHOUT ANY WARRANTY; without even the implied warranty of
' MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
' GNU General Public License for more details.
'
' You should have received a copy of the GNU General Public License
' along with Bigglesworth.  If not, see <http://www.gnu.org/licenses/>.

Imports System.Diagnostics.CodeAnalysis, System.Reflection ', log4net

''' <summary>
''' Signs outgoing MIME messages according to the DKIM protocol.
''' </summary>
''' <remarks></remarks>
Public NotInheritable Class dkimSigningRoutingAgent
    Inherits RoutingAgent

#Region " Shared Fields / Properties "

#End Region

#Region " Instance Fields / Properties "
    ''' <summary>
    ''' The object that knows how to sign messages.
    ''' </summary>
    ''' <remarks></remarks>
    Private _dkimSigner As IdkimSigner
#End Region

#Region " Instance Methods "
    ''' <summary>
    ''' Fired when Exchange has performed content conversion, if it was required.
    ''' The OnCategorizedMessage event is the last event that fires before the server
    ''' puts the message in the delivery queue. This means it's a good time to sign the
    ''' message, because it's unlikely that anything else will diddle with the message
    ''' and invalidate our signature. (Our transport agent will need to be the last to run,
    ''' though.)
    ''' </summary>
    ''' <param name="Source">The source.</param>
    ''' <param name="e">
    ''' The <see cref="Microsoft.Exchange.Data.Transport.Routing.QueuedMessageEventArgs"/>
    ''' instance containing the event data.
    ''' </param>
    ''' <remarks></remarks>
    <SuppressMessage("Microsoft.Design", "CA1031", Justification:="If an exception is thrown, then the message is eaten by Exchange. Better to catch the exception and write it to a log than to end up with a non-functioning MTA.")> _
    Private Sub WhenMessageCategorized(ByVal Source As CategorizedMessageEventSource, e As QueuedMessageEventArgs)
        Try
            SignMailItem(e.MailItem)
        Catch ex As Exception
            ' Log any errors.
            Dim lmessage As String = String.Format("{0} - {1} - {2}", My.Resources.dkimSigningRoutingAgent_SignFailed, ex.ToString, ex.Message)
            'Debug.WriteLine(lmessage)
            'My.Application.Log.WriteException(ex, TraceEventType.Error, lmessage)
            '_log.Error(My.Resources.dkimSigningRoutingAgent_SignFailed, ex)
            Logger.Log(Logger.Verbosity.Low, lmessage)
        End Try
    End Sub
    ''' <summary>
    ''' Signs the given mail item, if possible, according to the DKIM standard.
    ''' </summary>
    ''' <param name="MailItem">The mail item that is to be signed, if possible.</param>
    ''' <remarks></remarks>
    Private Sub SignMailItem(ByVal MailItem As MailItem)
        ' If the mail item is a "system message" then it will be read-only here,
        ' and we can't sign it. Additionally, if the message has a "TnefPart",
        ' then it is in a proprietary format used by Outlook and Exchange Server,
        ' which means we shouldn't bother signing it.
        If Logger.SetVerbosity >= Logger.Verbosity.Medium Then
            Dim lmessage As String = String.Format("Message deposited. - Received: {0} {1} - From: {2} - To: {3} - Subject: {4}", MailItem.DateTimeReceived.ToShortDateString, MailItem.DateTimeReceived.ToLongTimeString, MailItem.FromAddress, MailItem.Recipients.First.Address.ToString, MailItem.Message.Subject)
            'Debug.WriteLine(lmessage)
            'My.Application.Log.WriteEntry(lmessage, TraceEventType.Verbose)
            Logger.Log(Logger.Verbosity.Medium, lmessage)
            'Logger.Log("Ready to sign message!")
        End If
        If Not MailItem.Message.IsSystemMessage AndAlso MailItem.Message.TnefPart Is Nothing Then
            If Logger.SetVerbosity >= Logger.Verbosity.Debug Then Logger.Log(Logger.Verbosity.Debug, "Message signable!")
            Using linputStream = MailItem.GetMimeReadStream()
                If Logger.SetVerbosity >= Logger.Verbosity.Debug Then Logger.Log(Logger.Verbosity.Debug, "Got input stream!")
                If _dkimSigner.CanSign(linputStream) Then
                    If Logger.SetVerbosity >= Logger.Verbosity.Debug Then Logger.Log(Logger.Verbosity.Debug, "Capable of signing!")
                    Using loutputStream = MailItem.GetMimeWriteStream
                        'If Logger.SetVerbosity >= Logger.Verbosity.High Then
                        '    Dim lmessage = String.Format("Message handling. - Received: {0} {1} - From: {2} - To: {3} - Subject: {4}", MailItem.DateTimeReceived.ToShortDateString, MailItem.DateTimeReceived.ToLongTimeString, MailItem.FromAddress, MailItem.Recipients.First.Address.ToString, MailItem.Message.Subject)
                        '    'Debug.WriteLine(lmessage)
                        '    'My.Application.Log.WriteEntry(lmessage, TraceEventType.Verbose)
                        '    '_log.Debug(lmessage)
                        '    Logger.Log(Logger.Verbosity.High, lmessage)
                        'End If
                        linputStream.Seek(0, IO.SeekOrigin.Begin)
                        If Logger.SetVerbosity >= Logger.Verbosity.Debug Then Logger.Log(Logger.Verbosity.Debug, "Got output stream!")
                        _dkimSigner.Sign(linputStream, loutputStream)
                        If Logger.SetVerbosity >= Logger.Verbosity.Debug Then Logger.Log(Logger.Verbosity.Debug, "Message signed!")
                    End Using
                End If
            End Using
            If Logger.SetVerbosity >= Logger.Verbosity.Medium Then
                Dim lmessage = String.Format("Message signed. - Received: {0} {1} - From: {2} - To: {3} - Subject: {4}", MailItem.DateTimeReceived.ToShortDateString, MailItem.DateTimeReceived.ToLongTimeString, MailItem.FromAddress, MailItem.Recipients.First.Address.ToString, MailItem.Message.Subject)
                'Debug.WriteLine(lmessage)
                'My.Application.Log.WriteEntry(lmessage, TraceEventType.Verbose)
                '_log.Debug(lmessage)
                Logger.Log(Logger.Verbosity.Medium, lmessage)
            End If
        End If
    End Sub
#End Region

#Region " Constructors "
    ''' <summary>
    ''' Initializes a new instance of the <see cref="dkimSigningRoutingAgent"/> class.
    ''' </summary>
    ''' <param name="dkimSigner">The object that knows how to sign messages.</param>
    ''' <remarks></remarks>
    Public Sub New(ByVal dkimSigner As IdkimSigner)
        ' Load configuration from file.
        ConfigHandler.Reload()
        If dkimSigner Is Nothing Then Throw New ArgumentNullException("dkimSigner")
        _dkimSigner = dkimSigner
        ' What "Categorized" means in this sense, only the Exchange team knows.
        AddHandler OnCategorizedMessage, AddressOf WhenMessageCategorized
    End Sub
#End Region

End Class
