﻿
' This file is part of Bigglesworth.
'
' Bigglesworth is free software: you can redistribute it and/or modify
' it under the terms of the GNU General Public License as published by
' the Free Software Foundation, either version 3 of the License, or
' (at your option) any later version.
'
' Bigglesworth is distributed in the hope that it will be useful,
' but WITHOUT ANY WARRANTY; without even the implied warranty of
' MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
' GNU General Public License for more details.
'
' You should have received a copy of the GNU General Public License
' along with Bigglesworth.  If not, see <http://www.gnu.org/licenses/>.

Imports System.Configuration

''' <summary>
''' Handles moving configuration settings from the bigglesworth.dll.config file into memory.
''' </summary>
''' <remarks>
''' Libraries do not pay any attention to their xxxx.dll.config file by default. They use the
''' settings that were compiled into them. This may make sense for widely distributed dll's.
''' It makes no sense for this one though. So, here we load the settings from the file into
''' the settings that are actually used and stored in memory.
''' I prefer this to the direct access method that was originally used by Nicholas because I
''' think it makes for cleaner code throughout the rest of the library.
''' </remarks>
Public Class ConfigHandler

    ''' <summary>
    ''' An object by which to access our configuration settings from bigglesworth.dll.config.
    ''' </summary>
    ''' <remarks></remarks>
    Private Shared _config As Configuration
    ''' <summary>
    ''' A public accessor for the configuration object.
    ''' </summary>
    ''' <value></value>
    ''' <returns>A system.configuration.configuration object.</returns>
    ''' <remarks>Lazy loads the config.</remarks>
    Public Shared ReadOnly Property Config As Configuration
        Get
            If _config Is Nothing Then
                'Logger.Log(Logger.Verbosity.Low, GetType(ConfigHandler).Assembly.Location)
                _config = ConfigurationManager.OpenExeConfiguration(GetType(ConfigHandler).Assembly.Location)
            End If
            Return _config
        End Get
    End Property
    ''' <summary>
    ''' A simplified public accessor for the settings collection of the configuration object.
    ''' </summary>
    ''' <value></value>
    ''' <returns>A system.configuration.KeyValueConfigurationCollection object.</returns>
    ''' <remarks></remarks>
    Public Shared ReadOnly Property Settings As KeyValueConfigurationCollection
        Get
            Return Config.AppSettings.Settings
        End Get
    End Property

    ''' <summary>
    ''' Load the settings from the app.config file into the settings stored in memory for the dll.
    ''' </summary>
    ''' <remarks>
    ''' </remarks>
    Public Shared Sub Reload()
        ' Log our progress for debugging.
        Logger.Log(Logger.Verbosity.Debug, "Start Save!")
        ' Delete the current config so that it will be reloaded.
        _config = Nothing
        ' Take the setting from our config file and move them into the My.Settings system.
        My.Settings.Item("dkimDefaultSigner_Algorithm") = Settings("dkimDefaultSigner_Algorithm").Value
        My.Settings.Item("dkimDefaultSigner_HeadersToSign") = Settings("dkimDefaultSigner_HeadersToSign").Value
        My.Settings.Item("dkimDefaultSigner_Selector") = Settings("dkimDefaultSigner_Selector").Value
        My.Settings.Item("dkimDefaultSigner_Domain") = Settings("dkimDefaultSigner_Domain").Value
        My.Settings.Item("dkimDefaultSigner_PrivateKey") = Settings("dkimDefaultSigner_PrivateKey").Value
        My.Settings.Item("dkimDefaultSigner_Algorithm") = Settings("dkimDefaultSigner_Algorithm").Value
        'My.Settings.Item("LogPath") = Settings("LogPath").ToString ' For some reason setting this causes me permissions issues. I haven't quite figured out why yet.
        My.Settings.Item("LoggerVerbosity") = Settings("LoggerVerbosity").Value
        My.Settings.Save()
        ' Log our progress for debugging.
        Logger.Log(Logger.Verbosity.Debug, "End Save!")
    End Sub

End Class
