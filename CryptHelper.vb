﻿
' This file is part of Bigglesworth.
'
' Bigglesworth is free software: you can redistribute it and/or modify
' it under the terms of the GNU General Public License as published by
' the Free Software Foundation, either version 3 of the License, or
' (at your option) any later version.
'
' Bigglesworth is distributed in the hope that it will be useful,
' but WITHOUT ANY WARRANTY; without even the implied warranty of
' MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
' GNU General Public License for more details.
'
' You should have received a copy of the GNU General Public License
' along with Bigglesworth.  If not, see <http://www.gnu.org/licenses/>.

Imports System.IO, System.Security.Cryptography, System.Text

''' <summary>
''' Contains helper methods for retrieving encryption objects.
''' </summary>
''' <remarks></remarks>
Public Class CryptHelper

    ''' <summary>
    ''' The header for an RSA private key in PEM format.
    ''' </summary>
    ''' <remarks></remarks>
    Private Const PemRsaPrivateKeyHeader As String = "-----BEGIN RSA PRIVATE KEY-----"

    ''' <summary>
    ''' The footer for an RSA private key in PEM format.
    ''' </summary>
    ''' <remarks></remarks>
    Private Const PemRsaPrivateKeyFooter As String = "-----END RSA PRIVATE KEY-----"

    ''' <summary>
    ''' Attempts to get an instance of an RSACryptoServiceProvider from a
    ''' PEM-encoded RSA private key, commonly used by OpenSSL. You would think
    ''' that this would be built into the .Net framework, but oh well.
    ''' </summary>
    ''' <param name="EncodedKey">The PEM-encoded key.</param>
    ''' <returns>
    ''' The RSACryptoServiceProvider instance, which the caller is responsible
    ''' for disposing.
    ''' </returns>
    ''' <remarks></remarks>
    Public Shared Function GetProviderFromPemEncodedRsaPrivateKey(ByVal EncodedKey As String) As RSACryptoServiceProvider
        EncodedKey = EncodedKey.Trim
        If Not EncodedKey.StartsWith(PemRsaPrivateKeyHeader, StringComparison.Ordinal) OrElse Not EncodedKey.EndsWith(PemRsaPrivateKeyFooter, StringComparison.Ordinal) Then
            Throw New ArgumentException(My.Resources.CryptHelper_BadPemFormat, "EncodedKey")
        End If
        EncodedKey = EncodedKey.Substring(PemRsaPrivateKeyHeader.Length, EncodedKey.Length - PemRsaPrivateKeyHeader.Length - PemRsaPrivateKeyFooter.Length)
        Return GetProviderFromDerEncodedRsaPrivateKey(Convert.FromBase64String(EncodedKey.Trim()))
    End Function

    ''' <summary>
    ''' Attempts to get an instance of an RSACryptoServiceProvider from a DER-encoded
    ''' RSA private key, commonly used by OpenSSL. It's ripped pretty shamelessly from
    ''' http://www.jensign.com/opensslkey/opensslkey.cs.
    ''' </summary>
    ''' <param name="EncodedKey">The PEM-encoded key.</param>
    ''' <returns>The RSACryptoServiceProvider instance, whic the caller is responsible for disposing.</returns>
    ''' <remarks></remarks>
    Public Shared Function GetProviderFromDerEncodedRsaPrivateKey(ByVal EncodedKey() As Byte) As RSACryptoServiceProvider
        Dim lprovider As RSACryptoServiceProvider
        Using lstream As New MemoryStream(EncodedKey)
            Using lreader As New BinaryReader(lstream)
                Dim lcount = 0
                Try
                    ' The data is read in little endian order.
                    Select Case lreader.ReadUInt16()
                        Case &H8130
                            ' Advance 1 byte.
                            lreader.ReadByte()
                        Case &H8230
                            ' Advance 2 bytes.
                            lreader.ReadInt16()
                        Case Else
                            Return Nothing
                    End Select
                    ' Unsupported version number; we don't know how to read anything else.
                    If lreader.ReadUInt16() <> &H102 Then Return Nothing
                    If lreader.ReadByte() <> &H0 Then Return Nothing
                    Dim lparameters = New RSAParameters()
                    lparameters.Modulus = lreader.ReadBytes(ReadFieldLength(lreader))
                    lparameters.Exponent = lreader.ReadBytes(ReadFieldLength(lreader))
                    lparameters.D = lreader.ReadBytes(ReadFieldLength(lreader))
                    lparameters.P = lreader.ReadBytes(ReadFieldLength(lreader))
                    lparameters.Q = lreader.ReadBytes(ReadFieldLength(lreader))
                    lparameters.DP = lreader.ReadBytes(ReadFieldLength(lreader))
                    lparameters.DQ = lreader.ReadBytes(ReadFieldLength(lreader))
                    lparameters.InverseQ = lreader.ReadBytes(ReadFieldLength(lreader))
                    lprovider = New RSACryptoServiceProvider
                    lprovider.ImportParameters(lparameters)
                Catch ex As Exception
                    Throw New ArgumentException(My.Resources.CryptHelper_BadDerFormat, "EncodedKey", ex)
                Finally
                    lreader.Close()
                End Try
            End Using
        End Using
        Return lprovider
    End Function

    ''' <summary>
    ''' Gets the size of the next field in a DER-encoded RSA private key.
    ''' This is ripped shamelessly from http://www.jensign.com/opensslkey/opensslkey.cs
    ''' </summary>
    ''' <param name="Reader">The reader containing the key data.</param>
    ''' <returns>The length of the next RSA key field, in bytes.</returns>
    ''' <remarks></remarks>
    Private Shared Function ReadFieldLength(ByVal Reader As BinaryReader)
        Dim lbyte As Byte = &H0 ' The value of the last read byte.
        Dim lcount As Integer = 0 ' The length of the next field, in bytes.
        'We expect an integer.
        If Reader.ReadByte() <> &H2 Then Return 0
        lbyte = Reader.ReadByte()
        Select Case lbyte
            Case &H81
                ' Data size is in the next byte.
                lcount = Reader.ReadByte()
            Case &H82
                ' Data size is in the next two bytes.
                Dim lhigh = Reader.ReadByte()
                Dim llow = Reader.ReadByte()
                lcount = BitConverter.ToInt32(New Byte() {llow, lhigh, &H0, &H0}, 0)
            Case Else
                ' We already have the data size.
                lcount = lbyte
        End Select
        ' Remove the high-order zeroes in the data.
        While Reader.ReadByte = &H0
            lcount -= 1
        End While
        ' The last read byte wasn't a zero, so back up a byte
        ' for teh next invocation of this function.
        Reader.BaseStream.Seek(-1, SeekOrigin.Current)
        Return lcount
    End Function

End Class
