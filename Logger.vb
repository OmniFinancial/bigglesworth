﻿
' This file is part of Bigglesworth.
'
' Bigglesworth is free software: you can redistribute it and/or modify
' it under the terms of the GNU General Public License as published by
' the Free Software Foundation, either version 3 of the License, or
' (at your option) any later version.
'
' Bigglesworth is distributed in the hope that it will be useful,
' but WITHOUT ANY WARRANTY; without even the implied warranty of
' MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
' GNU General Public License for more details.
'
' You should have received a copy of the GNU General Public License
' along with Bigglesworth.  If not, see <http://www.gnu.org/licenses/>.

''' <summary>
''' Class used to log events.
''' </summary>
''' <remarks>
''' This class was created in lieu of using the built in My.Application.Log or log4net systems because I had permissions issues while using those systems.
''' Debugging this library is particularly difficult because it is normally meant to be run by Exchange. This makes it rather difficult to determine what
''' the permissions issue is caused by. I had so much trouble with it that I finally just gave up and wrote my own little logger that works well enough.
''' </remarks>
Public Class Logger

    ''' <summary>
    ''' Shared field in which to store the currently set logging verbosity of the application.
    ''' </summary>
    ''' <remarks>
    ''' The set logging verbosity is used to determine what calls to the Log method are heeded.
    ''' Uses a nested enumeration as data type.
    ''' </remarks>
    Private Shared _setVerbosity As Verbosity = Verbosity.Low
    ''' <summary>
    ''' Shared read-only property by which objects may obtain the currently set logging verbosity of the application.
    ''' </summary>
    ''' <value></value>
    ''' <returns>Verbosity</returns>
    ''' <remarks>
    ''' The set logging verbosity is used to determine what calls to the Log method are heeded.
    ''' Uses a nested enumeration as data type.
    ''' </remarks>
    Public Shared ReadOnly Property SetVerbosity As Verbosity
        Get
            Return _setVerbosity
        End Get
    End Property

    ''' <summary>
    ''' Method by which the logger is initialized.
    ''' </summary>
    ''' <param name="Verbosity">Nested enumeration "Verbosity".</param>
    ''' <remarks>Initialize the logger, passing the desired logging verbosity as an enumeration.</remarks>
    Public Overloads Shared Sub Initialize(ByVal Verbosity As Verbosity)
        Try
            _setVerbosity = Verbosity
        Catch ex As Exception
        End Try
    End Sub
    ''' <summary>
    ''' Method by which the logger is initialized.
    ''' </summary>
    ''' <param name="Verbosity">String that parses to nested enumeration "Verbosity".</param>
    ''' <remarks>Initialize the logger, passing the desired logging verbosity as a string.</remarks>
    Public Overloads Shared Sub Initialize(ByVal Verbosity As String)
        Try
            Initialize([Enum].Parse(GetType(Verbosity), Verbosity, True))
        Catch ex As Exception
        End Try
    End Sub

    ''' <summary>
    ''' Method by which log entries are made.
    ''' </summary>
    ''' <param name="Verbosity">The verbosity at which this log call will be implemented.</param>
    ''' <param name="Message">The string message which will be written to the log file if this call is implemented.</param>
    ''' <remarks>
    ''' If the verbosity defined in the call is less than or equal to the set verbosity for the logger
    ''' as defined in the "SetVerbosity" property, then the call is implemented.
    ''' </remarks>
    Public Shared Sub Log(ByVal Verbosity As Verbosity, ByVal Message As String)
        ' Determine if the verbosity of the call is <= the set verbosity we have stored.
        ' If so, then implement the call.
        If Verbosity <= _setVerbosity Then
            ' Implement the log call.
            ' Determine the log directory from the configuration settings.
            Dim ldirName As String = My.Settings.LogPath
            ' Determine if the log directory exists.
            ' If the log directory does not exist, create it.
            ' This functionality has been removed because it 
            'If Not My.Computer.FileSystem.DirectoryExists(ldirName) Then My.Computer.FileSystem.CreateDirectory(ldirName)
            ' Determine the datetime with which we will stamp our log entry.
            Dim lstamp As Date = Date.Now
            ' Start creating our log file name using the year derived from lstamp.
            Dim lfileName As String = lstamp.Year.ToString
            ' Determine if our month string will need zero filled. If so, add a zero onto lfileName.
            If lstamp.Month.ToString.Length < 2 Then lfileName += "0"
            ' Continue building our log file name using the month derived from lstamp.
            lfileName += lstamp.Month.ToString
            ' Determine if our day string will need zero filled. If so, add a zero onto lfileName.
            If lstamp.Day.ToString.Length < 2 Then lfileName += "0"
            ' Continue building our log file name using the day derived from lstamp.
            lfileName += lstamp.Day.ToString
            ' Format our log file name using a full path and extension.
            lfileName = String.Format("{0}{1}.log", ldirName, lfileName)
            ' A place to store a carriage return / line feed if we are appending to an existing log file rather than creating a new one.
            ' This string will go at the beginning of our log line.
            Dim llineHead = ""
            ' Determine if we are appending to an existing log file, or creating a new one.
            ' If we are appending to an existing file, then set llineHead to vbCrLf.
            ' This way we create a new line upon which to place our message, when we are appending to an existing file.
            If My.Computer.FileSystem.FileExists(lfileName) Then llineHead = vbCrLf
            ' Write our log line into the log file defined by lfileName using the defined format, datetime stamp, verbosity level, and message.
            My.Computer.FileSystem.WriteAllText(lfileName, String.Format("{0}{1} - {2} - {3}", llineHead, lstamp.ToLongTimeString, Verbosity.ToString, Message), True)
        End If
    End Sub

    ''' <summary>
    ''' The pre-defined verbosity levels that have been defined for the logger.
    ''' </summary>
    ''' <remarks>Room for expansion has been left around and between these pre-defined levels.</remarks>
    Public Enum Verbosity
        ''' <summary>
        ''' No messages will be written to the log file.
        ''' </summary>
        None = 30
        ''' <summary>
        ''' Only error messages will be written to the log file.
        ''' </summary>
        Low = 40
        ''' <summary>
        ''' Error messages and major milestone informational messages will be written to the log file.
        ''' </summary>
        Medium = 50
        ''' <summary>
        ''' Error messages, major milestone informational messages, and minor milestone informational messages will be written to the log file.
        ''' </summary>
        High = 60
        ''' <summary>
        ''' Error messages, major mileston informational messages, minor milestone informational messages, and debug level (nearly line by line) messages will be written to the log file.
        ''' </summary>
        Debug = 70
    End Enum

End Class
