﻿
' This file is part of Bigglesworth.
'
' Bigglesworth is free software: you can redistribute it and/or modify
' it under the terms of the GNU General Public License as published by
' the Free Software Foundation, either version 3 of the License, or
' (at your option) any later version.
'
' Bigglesworth is distributed in the hope that it will be useful,
' but WITHOUT ANY WARRANTY; without even the implied warranty of
' MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
' GNU General Public License for more details.
'
' You should have received a copy of the GNU General Public License
' along with Bigglesworth.  If not, see <http://www.gnu.org/licenses/>.

Imports Microsoft.Exchange.Data.TextConverters
Imports System.IO

''' <summary>
''' An object that knows how to sign a MIME message according to the DKIM standard.
''' </summary>
''' <remarks></remarks>
Public Interface IdkimSigner
    Inherits IDisposable

    ''' <summary>
    ''' Return a value indicating whether the unsigned MIME message in the given
    ''' stream can be signed.
    ''' </summary>
    ''' <param name="InputStream">The input stream.</param>
    ''' <returns>The output stream.</returns>
    ''' <remarks></remarks>
    Function CanSign(ByVal InputStream As Stream) As Boolean

    ''' <summary>
    ''' Writes a signed version of the unsigned MIME message from the input stream
    ''' to the output stream.
    ''' </summary>
    ''' <param name="InputStream">The input stream.</param>
    ''' <param name="OutputStream">The output stream.</param>
    ''' <remarks></remarks>
    Sub Sign(ByVal InputStream As Stream, ByVal OutputStream As Stream)

End Interface
