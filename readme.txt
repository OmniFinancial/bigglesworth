UPDATE: I would really recommend using the library being created here: https://github.com/Pro/dkim-exchange
It's being actively worked on and has been updated to work on more versions of Exchange etc.



﻿This software known as "Bigglesworth" is hosted at https://bitbucket.org/OmniFinancial/bigglesworth
It is written by Tory Netherton. It is a reimplementation of a DKIM signing transport agent for
Microsoft Exchange written by Nicholas Piasecki and published at the following web address.
http://nicholas.piasecki.name/blog/2010/12/dkim-signing-outbound-messages-in-exchange-server-2007/

He wrote his version in C#, published it (and his explanation of it) on his web site and made it
public domain. I reimplemented it in vb.net and hosted it on BitBucket primarily because I
believed I would better understand it by writing it myself than I would simply implementing
what he had written. It also gave me a chance to write something small and useful in vb.net. In
addition to that though, I thought that it would be good to get an open source version of it hosted
somewhere that it would be easy for others to contribute to and improve. I know there is a great
deal that could be done to improve what I have here. I welcome such contributions. I also would
like to see more open source projects implemented in vb.net. It's a great language that often gets
overlooked or unfairly belittled.

I enjoy writing in vb.net and this gave me a good opportunity to do so. I licensed this work using
the GNU General Public License of which a copy is included in this project (gpl-3.0.txt).

This library will sign messages outgoing from Microsoft Exchange using the DKIM standard. There is
currently no native ability to do so in Exchange. Your only commercial options are pretty sketchy.
That leaves writing your own or just implementing this or the one written by Nicholas Piasecki.

A great many Exchange servers need this functionality. Here it is. It's simple. Use it. The world
will be a better less spammy place.

Debugging this library was particularly troublesome as it is normally meant to be run by Exchange.
Unfortunatley, you can't just run the debugger. I tried initially to write unit tests for it but
found that it would be extremely difficult to in that I would have to implement major classes from
the Exchange library in order to do so. Therefore, I just used the logger that I built for it and
it was difficult but got the job done.

Speaking of the logger. That was created because when the library is called by Exchange using
other logging mechanisms, such as My.Application.Log or log4net, I had permissions issues that I
was not easily able to determine the cause. This is primarily thanks to the lack of decent
debugging.

Thanks immensely to Nicholas for the work he did. Without him I would have been lost and probably
purchasing some terrible software from some pretty sketchy source.

Please feel free to contact me via the BitBucket site if you have any questions or comments.