﻿
' This file is part of Bigglesworth.
'
' Bigglesworth is free software: you can redistribute it and/or modify
' it under the terms of the GNU General Public License as published by
' the Free Software Foundation, either version 3 of the License, or
' (at your option) any later version.
'
' Bigglesworth is distributed in the hope that it will be useful,
' but WITHOUT ANY WARRANTY; without even the implied warranty of
' MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
' GNU General Public License for more details.
'
' You should have received a copy of the GNU General Public License
' along with Bigglesworth.  If not, see <http://www.gnu.org/licenses/>.

Imports System.Collections.Generic, System.Globalization, System.IO, System.Linq, System.Security.Cryptography, System.Text, System.Text.RegularExpressions

''' <summary>
''' Signs MIME messages according to the DKIM standard.
''' </summary>
''' <remarks></remarks>
Public Class dkimDefaultSigner
    Implements IdkimSigner

#Region " Shared Fields / Properties "
    ''' <summary>
    ''' The sentinel for a header separator.
    ''' </summary>
    ''' <remarks></remarks>
    Private Shared ReadOnly HeaderSeparatorSentinel As Byte() = Encoding.ASCII.GetBytes(vbCrLf + vbCrLf)
#End Region

#Region " Instance Fields / Properties "
    ''' <summary>
    ''' The RSA crypto service provider.
    ''' </summary>
    ''' <remarks></remarks>
    Private _cryptoProvider As RSACryptoServiceProvider
    ''' <summary>
    ''' A value indicating whether the instance of the signer has been disposed.
    ''' </summary>
    ''' <remarks></remarks>
    Private _disposed As Boolean
    ''' <summary>
    ''' The domain that has the TXT record of the public key.
    ''' </summary>
    ''' <remarks></remarks>
    Private _domain As String
    ''' <summary>
    ''' The headers that should be a part of the dkim signature, if present in the message.
    ''' </summary>
    ''' <remarks></remarks>
    Private _eligibleHeaders As HashSet(Of String)
    ''' <summary>
    ''' The hash algorithm that is to be employed.
    ''' </summary>
    ''' <remarks></remarks>
    Private _hashAlgorithm As HashAlgorithm
    ''' <summary>
    ''' The DKIM code for the hash algorithm that is to be employed.
    ''' </summary>
    ''' <remarks></remarks>
    Private _hashAlgorithmDKIMCode As String
    ''' <summary>
    ''' The crypto provider code for the hash algorithm that is toe be employed.
    ''' </summary>
    ''' <remarks></remarks>
    Private _hashAlgorithmCryptoCode As String
    ''' <summary>
    ''' The selector that denotes which public key to use.
    ''' </summary>
    ''' <remarks></remarks>
    Private _selector As String
#End Region

#Region " Instance Methods "
    ''' <summary>
    ''' Returns a value indicating whether the unsigned MIME message in the given
    ''' stream can be signed. In this case, we iterate until we see the "From:" header,
    ''' and then we only sign it if our domain matches the domain of the "From:" address.
    ''' </summary>
    ''' <param name="InputStream">The input stream.</param>
    ''' <returns>The output stream.</returns>
    ''' <remarks></remarks>
    Public Function CanSign(InputStream As Stream) As Boolean Implements IdkimSigner.CanSign
        If _disposed Then Throw New ObjectDisposedException("DomainKeysSigner")
        If InputStream Is Nothing Then Throw New ArgumentNullException("InputStream")
        Dim lcanSign = False
        Dim lreader As New StreamReader(InputStream)
        Dim lline = lreader.ReadLine()
        While lline IsNot Nothing
            Dim lheader As String
            Dim lheaderParts() As String
            ' We've reached the end of the headers (headers are
            ' separated from the body by a blank line).
            If lline.Length = 0 Then Exit While
            ' Read a line. Because a header can be continued onto
            ' subsequent lines, we have to keep reading lines until we
            ' run into the "end-of-headers" marker (an empty line) or
            ' another line that doesn't begin with a whitespace character.
            lheader = lline + vbCrLf
            lline = lreader.ReadLine()
            While Not String.IsNullOrEmpty(lline) AndAlso (lline.StartsWith(vbTab, StringComparison.Ordinal) OrElse lline.StartsWith(" ", StringComparison.Ordinal))
                lheader += lline + vbCrLf
                lline = lreader.ReadLine()
            End While
            ' Extract the name of the header. Then store the full header
            ' in the dictionary. We do this because DKIM mandates that we
            ' only sign the LAST instance of any header that occurs.
            lheaderParts = lheader.Split(New Char() {":"}, 2)
            If lheaderParts.Length = 2 Then
                Dim lheaderName As String = lheaderParts(0)
                If lheaderName.Equals("From", StringComparison.OrdinalIgnoreCase) Then
                    ' We don't break here because we want to read the bottom-most
                    ' instance of the "From:" header (there should be only one, but
                    ' if there are multiple, it's the last one that matters).
                    lcanSign = lheader.ToUpperInvariant.Contains("@" + _domain.ToUpperInvariant())
                End If
            End If
        End While
        If Logger.SetVerbosity >= Logger.Verbosity.High Then
            Dim lmessage As String = My.Resources.dkimDefaultSigner_Log_CanSign
            'Debug.WriteLine(lmessage)
            'My.Application.Log.WriteEntry(lmessage)
            Logger.Log(Logger.Verbosity.High, lmessage)
        End If
        Return lcanSign
    End Function

    ''' <summary>
    ''' Writes a signed version of the unsigned MIME message from the input stream
    ''' to the output stream.
    ''' </summary>
    ''' <param name="InputStream">The input stream.</param>
    ''' <param name="OutputStream">The output stream.</param>
    ''' <remarks></remarks>
    Public Sub Sign(InputStream As Stream, OutputStream As Stream) Implements IdkimSigner.Sign
        If Logger.SetVerbosity >= Logger.Verbosity.Debug Then Logger.Log(Logger.Verbosity.Debug, "Signer: Ready to sign message!")
        If _disposed Then Throw New ObjectDisposedException("DomainKeysSigner")
        If Logger.SetVerbosity >= Logger.Verbosity.Debug Then Logger.Log(Logger.Verbosity.Debug, "Signer: Not disposed!")
        If InputStream Is Nothing Then Throw New ArgumentNullException("InputStream")
        If Logger.SetVerbosity >= Logger.Verbosity.Debug Then Logger.Log(Logger.Verbosity.Debug, "Signer: Input stream exists!")
        If OutputStream Is Nothing Then Throw New ArgumentNullException("OutputStream")
        If Logger.SetVerbosity >= Logger.Verbosity.Debug Then Logger.Log(Logger.Verbosity.Debug, "Signer: Output stream exists!")
        Dim lbodyHash = GetBodyHash(InputStream)
        If Logger.SetVerbosity >= Logger.Verbosity.Debug Then Logger.Log(Logger.Verbosity.Debug, "Signer: Body hashed!")
        Dim lunsignedDKIMHeader = GetUnsignedDKIMHeader(lbodyHash)
        If Logger.SetVerbosity >= Logger.Verbosity.Debug Then Logger.Log(Logger.Verbosity.Debug, "Signer: Got unsigned header!")
        Dim lcanonicalizedHeaders = GetCanonicalizedHeaders(InputStream)
        If Logger.SetVerbosity >= Logger.Verbosity.Debug Then Logger.Log(Logger.Verbosity.Debug, "Signer: Got canonicalized headers!")
        Dim lsignedDKIMHeader = GetSignedDKIMHeader(lunsignedDKIMHeader, lcanonicalizedHeaders)
        If Logger.SetVerbosity >= Logger.Verbosity.Debug Then Logger.Log(Logger.Verbosity.Debug, "Signer: Got signed header!")
        WriteSignedMIMEMessage(InputStream, OutputStream, lsignedDKIMHeader)
        If Logger.SetVerbosity >= Logger.Verbosity.Debug Then Logger.Log(Logger.Verbosity.Debug, "Signer: Wrote signed message!")
        If Logger.SetVerbosity >= Logger.Verbosity.High Then
            Dim lmessage As String = My.Resources.dkimDefaultSigner_Log_Sign
            'Debug.WriteLine(lmessage)
            'My.Application.Log.WriteEntry(lmessage)
            Logger.Log(Logger.Verbosity.High, lmessage)
        End If
    End Sub

    ''' <summary>
    ''' Test to see if the candidate sequence exists at the given position in the stream.
    ''' </summary>
    ''' <param name="Stream">The stream.</param>
    ''' <param name="Candidate">The candidate we're looking for.</param>
    ''' <returns>Whether the candidate exists at the current position in the stream.</returns>
    ''' <remarks></remarks>
    Private Shared Function IsMatch(ByVal Stream As Stream, ByVal Candidate() As Byte) As Boolean
        If Candidate.Length > Stream.Length - Stream.Position Then Return False
        For i = 0 To Candidate.Length - 1
            If Stream.ReadByte() <> Candidate(i) Then Return False
        Next
        Return True
    End Function

    ''' <summary>
    ''' Computes the hash of the body of the MIME message (represented by the given stream)
    ''' and returned as a base64-encoded string. The stream position is reset back to the start
    ''' of the stream.
    ''' </summary>
    ''' <param name="stream">The stream that represents the MIME message.</param>
    ''' <returns>The base64-encoded hash of the body.</returns>
    ''' <remarks></remarks>
    Private Function GetBodyHash(Stream As Stream) As String
        Dim lbodyBytes() As Byte
        Dim lbodyText As String
        Dim lhashText As String
        Dim lindex As Int64 = -1
        Stream.Seek(0, SeekOrigin.Begin)
        While Stream.Position < Stream.Length
            If IsMatch(Stream, HeaderSeparatorSentinel) Then
                lindex = Stream.Position
                Exit While
            End If
        End While
        If lindex < 0 Then Throw New ArgumentException("The stream did not have a MIME body.", "Stream")
        ' We have to ignore all empty lines at the end of the message body.
        ' This means we have to read the whole body and fix up the end of the
        ' body if necessary.
        lbodyText = New StreamReader(Stream).ReadToEnd()
        lbodyText = Regex.Replace(lbodyText, "(\r?\n)*$", String.Empty)
        lbodyText += vbCrLf
        lbodyBytes = Encoding.ASCII.GetBytes(lbodyText)
        lhashText = Convert.ToBase64String(_hashAlgorithm.ComputeHash(lbodyBytes))
        If Logger.SetVerbosity >= Logger.Verbosity.High Then
            Dim lmessage As String = String.Format("{0} =|{1}|= {2}", My.Resources.dkimDefaultSigner_Log_GetBodyHash, lbodyText, lhashText)
            'Debug.WriteLine(lmessage)
            'My.Application.Log.WriteEntry(lmessage)
            Logger.Log(Logger.Verbosity.High, lmessage)
        End If
        Stream.Seek(0, SeekOrigin.Begin)
        Return lhashText
    End Function

    ''' <summary>
    ''' Builds an unsigned DKIM-Signature header. Note that the returned
    ''' header will NOT have a CRLF at the end.
    ''' </summary>
    ''' <param name="BodyHash">The hash of the body.</param>
    ''' <returns>The unsigned DKIM-Signature header.</returns>
    ''' <remarks></remarks>
    Private Function GetUnsignedDKIMHeader(ByVal BodyHash As String) As String
        Dim lresult = String.Format(CultureInfo.InvariantCulture _
                                    , "DKIM-Signature: v=1; a={0}; s={1}; d={2}; c=simple/simple; q=dns/txt; h={3}; bh={4}; b=;" _
                                    , _hashAlgorithmDKIMCode _
                                    , _selector _
                                    , _domain _
                                    , String.Join(" : ", _eligibleHeaders.OrderBy(Function(x) x, StringComparer.Ordinal).ToArray()) _
                                    , BodyHash)
        If Logger.SetVerbosity >= Logger.Verbosity.High Then
            Dim lmessage As String = My.Resources.dkimDefaultSigner_Log_GetUnsignedDKIMHeader + lresult
            'Debug.WriteLine(lmessage)
            'My.Application.Log.WriteEntry(lmessage)
            Logger.Log(Logger.Verbosity.High, lmessage)
        End If
        Return lresult
    End Function

    ''' <summary>
    ''' Runs through the MIME stream and sucks out the headers that should be part
    ''' of the DKIM signature. The headers here have their whitespace preserved and
    ''' all terminate with CRLF. The stream position is returned to the start of the
    ''' stream.
    ''' </summary>
    ''' <param name="Stream">The stream containing the MIME message.</param>
    ''' <returns>An enumerable of the headers that should be a part of the signature.</returns>
    ''' <remarks></remarks>
    Private Function GetCanonicalizedHeaders(Stream As Stream) As IEnumerable(Of String)
        Dim lheaderNameToLineMap As New Dictionary(Of String, String)
        Stream.Seek(0, SeekOrigin.Begin)
        Dim lreader As New StreamReader(Stream)
        Dim lline = lreader.ReadLine
        While lline IsNot Nothing
            Dim lheader As String
            Dim lheaderParts() As String
            ' We've reached the end of the headers (headers are
            ' separated from the body by a blank line).
            If lline.Length = 0 Then Exit While
            ' Read a line. Because a header can be continued onto
            ' subsequent lines, we have to keep reading lines until we
            ' run into the "end-of-headers" marker (an empty line) or another
            ' line that doesn't begin with a whitespace character.
            lheader = lline + vbCrLf
            lline = lreader.ReadLine()
            While Not String.IsNullOrEmpty(lline) AndAlso (lline.StartsWith(vbTab, StringComparison.Ordinal) OrElse lline.StartsWith(" ", StringComparison.Ordinal))
                lheader += lline + vbCrLf
                lline = lreader.ReadLine()
            End While
            ' Extract the name of the header. Then store the full header
            ' in the dictionary. We do this because DKIM mandates that we
            ' only sign the LAST instance of any header that occurs.
            lheaderParts = lheader.Split(New Char() {":"}, 2)
            If lheaderParts.Length = 2 Then
                Dim lheaderName As String = lheaderParts(0)
                ' We only want to sign the header if we were told to sign it!
                If _eligibleHeaders.Contains(lheaderName, StringComparer.OrdinalIgnoreCase) Then lheaderNameToLineMap(lheaderName) = lheader
            End If
        End While
        Stream.Seek(0, SeekOrigin.Begin)
        Dim lresult = lheaderNameToLineMap.Values.OrderBy(Function(x) x, StringComparer.Ordinal)
        If Logger.SetVerbosity >= Logger.Verbosity.High Then
            Dim lmessage As String = My.Resources.dkimDefaultSigner_Log_GetCanonicalizedHeaders
            For Each iheader In lresult
                lmessage += " " + iheader
            Next
            'Debug.WriteLine(lmessage)
            'My.Application.Log.WriteEntry(lmessage)
            Logger.Log(Logger.Verbosity.High, lmessage)
        End If
        Return lresult
    End Function

    ''' <summary>
    ''' Gets the version of the DKIM-Signature header with the signature appended, along with
    ''' the CRLF.
    ''' </summary>
    ''' <param name="UnsignedDKIMHeader">The unsigned DKIM header, to use as a template.</param>
    ''' <param name="CanonicalizedHeaders">The headers to be included as part of the signature.</param>
    ''' <returns>The signed DKIM-Signature header.</returns>
    ''' <remarks></remarks>
    Private Function GetSignedDKIMHeader(ByVal UnsignedDKIMHeader As String, ByVal CanonicalizedHeaders As IEnumerable(Of String)) As String
        Dim lsignatureBytes() As Byte
        Dim lsignatureText As String
        Dim lsignedDkimHeader As StringBuilder
        Using lstream As New MemoryStream
            Using lwriter As New StreamWriter(lstream)
                For Each iheader In CanonicalizedHeaders
                    lwriter.Write(iheader)
                Next
                lwriter.Write(UnsignedDKIMHeader)
                lwriter.Flush()
                lstream.Seek(0, SeekOrigin.Begin)
                ' Why not pass this.hashAlgorithm here, since we already have it? If we're supporting
                ' Exchange 2007, then we're stuck on CLR 2.0. The SHA-256 functionality was added in
                ' .NET 3.5 SP1, but it was done in such a way that the switch statement used internally
                ' by the Crypto .NET classes won't recognize the new SHA256CryptoServiceProvider type.
                ' So, we have to use the string method instead. More details available at
                ' http://blogs.msdn.com/b/shawnfa/archive/2008/08/25/using-rsacryptoserviceprovider-for-rsa-sha256-signatures.aspx
                lsignatureBytes = _cryptoProvider.SignData(lstream, _hashAlgorithmCryptoCode)
            End Using
        End Using
        lsignatureText = Convert.ToBase64String(lsignatureBytes)
        lsignedDkimHeader = New StringBuilder(UnsignedDKIMHeader.Substring(0, UnsignedDKIMHeader.Length - 1))
        lsignedDkimHeader.AppendLine(lsignatureText + ";")
        If Logger.SetVerbosity >= Logger.Verbosity.High Then
            Dim lmessage As String = String.Format("{0} {1} | {2}", My.Resources.dkimDefaultSigner_Log_GetSignedDKIMHeader, lsignatureText, lsignedDkimHeader)
            'Debug.WriteLine(lmessage)
            'My.Application.Log.WriteEntry(lmessage)
            Logger.Log(Logger.Verbosity.High, lmessage)
        End If
        Return lsignedDkimHeader.ToString
    End Function

    ''' <summary>
    ''' Writes the message containing the signed DKIM-Signature header to the output stream.
    ''' </summary>
    ''' <param name="Input">The steam containing the original MIME message.</param>
    ''' <param name="Output">The stream containing the output MIME message.</param>
    ''' <param name="SignedDKIMHeader">The signed DKIM-Signature header.</param>
    ''' <remarks></remarks>
    Private Shared Sub WriteSignedMIMEMessage(ByVal Input As Stream, ByVal Output As Stream, ByVal SignedDKIMHeader As String)
        Dim lbytesRead As Integer
        Dim lstreamBuffer() = New Byte(1023) {}
        Dim lheaderBuffer() = Encoding.ASCII.GetBytes(SignedDKIMHeader)
        Input.Seek(0, SeekOrigin.Begin)
        Output.Write(lheaderBuffer, 0, lheaderBuffer.Length)
        lbytesRead = Input.Read(lstreamBuffer, 0, lstreamBuffer.Length)
        While lbytesRead > 0
            If Logger.SetVerbosity >= Logger.Verbosity.Debug Then Logger.Log(Logger.Verbosity.Debug, "Buffer Pass!")
            Output.Write(lstreamBuffer, 0, lbytesRead)
            lbytesRead = Input.Read(lstreamBuffer, 0, lstreamBuffer.Length)
        End While
        'While (lbytesRead = Input.Read(lstreamBuffer, 0, lstreamBuffer.Length)) > 0
        '    Output.Write(lstreamBuffer, 0, lbytesRead)
        'End While
        If Logger.SetVerbosity >= Logger.Verbosity.High Then
            Dim lmessage As String = My.Resources.dkimDefaultSigner_Log_WriteSignedMIMEMessage + SignedDKIMHeader
            'Debug.WriteLine(lmessage)
            'My.Application.Log.WriteEntry(lmessage)
            Logger.Log(Logger.Verbosity.High, lmessage)
        End If
    End Sub
#End Region

#Region " Constructors "
    Public Sub New(ByVal SignatureKind As dkimAlgorithmKind, ByVal HeadersToSign As IEnumerable(Of String), Selector As String, Domain As String, EncodedKey As String)
        If EncodedKey Is Nothing OrElse EncodedKey.Trim.Length = 0 Then Throw New ArgumentNullException("EncodedKey")
        If Selector Is Nothing OrElse Selector.Trim.Length = 0 Then Throw New ArgumentNullException("Selector")
        If Domain Is Nothing OrElse Domain.Trim.Length = 0 Then Throw New ArgumentNullException("Domain")

        _cryptoProvider = CryptHelper.GetProviderFromPemEncodedRsaPrivateKey(EncodedKey)
        _domain = Domain
        _selector = Selector

        Select Case SignatureKind
            Case dkimAlgorithmKind.RsaSha1
                _hashAlgorithm = New SHA1CryptoServiceProvider
                _hashAlgorithmCryptoCode = "SHA1"
                _hashAlgorithmDKIMCode = "rsa-sha1"
            Case dkimAlgorithmKind.RsaSha256
                _hashAlgorithm = New SHA256CryptoServiceProvider
                _hashAlgorithmCryptoCode = "SHA256"
                _hashAlgorithmDKIMCode = "rsa-sha256"
            Case Else
                Throw New ArgumentOutOfRangeException("SignatureKind")
        End Select

        _eligibleHeaders = New HashSet(Of String)

        If HeadersToSign IsNot Nothing Then
            For Each iheaderToSign In HeadersToSign
                _eligibleHeaders.Add(iheaderToSign.Trim())
            Next
        End If

        If Logger.SetVerbosity >= Logger.Verbosity.High Then
            Dim lmessage As String = My.Resources.dkimDefaultSigner_Log_New + String.Format("{0} - {1} - {2} - {3}", SignatureKind.ToString, HeadersToSign.ToString, Selector, Domain)
            'Debug.WriteLine(lmessage)
            'My.Application.Log.WriteEntry(lmessage)
            Logger.Log(Logger.Verbosity.High, lmessage)
        End If

    End Sub
#End Region

#Region "IDisposable Support"
    Private disposedValue As Boolean ' To detect redundant calls

    ' IDisposable
    ''' <summary>
    ''' Releases unmanaged and (optionally) managed resources.
    ''' </summary>
    ''' <param name="disposing">
    ''' <c>true</c> to release both managed and unmanaged resources;
    ''' <c>false</c> to release only unmanaged resources.
    ''' </param>
    ''' <remarks></remarks>
    Protected Overridable Sub Dispose(disposing As Boolean)
        If Not Me.disposedValue Then
            If disposing Then
                If _cryptoProvider IsNot Nothing Then
                    _cryptoProvider.Clear()
                    _cryptoProvider = Nothing
                End If
                If _hashAlgorithm IsNot Nothing Then
                    _hashAlgorithm.Clear()
                    _hashAlgorithm = Nothing
                End If
                _disposed = True
                ' TODO: dispose managed state (managed objects).
            End If

            ' TODO: free unmanaged resources (unmanaged objects) and override Finalize() below.
            ' TODO: set large fields to null.
        End If
        Me.disposedValue = True
    End Sub

    ' TODO: override Finalize() only if Dispose(ByVal disposing As Boolean) above has code to free unmanaged resources.
    'Protected Overrides Sub Finalize()
    '    ' Do not change this code.  Put cleanup code in Dispose(ByVal disposing As Boolean) above.
    '    Dispose(False)
    '    MyBase.Finalize()
    'End Sub

    ' This code added by Visual Basic to correctly implement the disposable pattern.
    Public Sub Dispose() Implements IDisposable.Dispose
        ' Do not change this code.  Put cleanup code in Dispose(ByVal disposing As Boolean) above.
        Dispose(True)
        GC.SuppressFinalize(Me)
    End Sub
#End Region

End Class
