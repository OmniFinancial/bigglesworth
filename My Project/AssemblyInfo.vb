﻿
' This file is part of Bigglesworth.
'
' Bigglesworth is free software: you can redistribute it and/or modify
' it under the terms of the GNU General Public License as published by
' the Free Software Foundation, either version 3 of the License, or
' (at your option) any later version.
'
' Bigglesworth is distributed in the hope that it will be useful,
' but WITHOUT ANY WARRANTY; without even the implied warranty of
' MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
' GNU General Public License for more details.
'
' You should have received a copy of the GNU General Public License
' along with Bigglesworth.  If not, see <http://www.gnu.org/licenses/>.

Imports System
Imports System.Reflection
Imports System.Runtime.InteropServices

' General Information about an assembly is controlled through the following 
' set of attributes. Change these attribute values to modify the information
' associated with an assembly.

' Review the values of the assembly attributes

<Assembly: AssemblyTitle("Bigglesworth")> 
<Assembly: AssemblyDescription("DKIM Signing Microsoft Exchange Transfer Agent")> 
<Assembly: AssemblyCompany("Omni Financial")> 
<Assembly: AssemblyProduct("Bigglesworth")> 
<Assembly: AssemblyCopyright("Copyright © Omni Financial 2012")> 
<Assembly: AssemblyTrademark("")> 

<Assembly: ComVisible(False)>

'The following GUID is for the ID of the typelib if this project is exposed to COM
<Assembly: Guid("bc2835ce-cd13-49c5-b4a9-6fa50f30b34c")> 

' Version information for an assembly consists of the following four values:
'
'      Major Version
'      Minor Version 
'      Build Number
'      Revision
'
' You can specify all the values or you can default the Build and Revision Numbers 
' by using the '*' as shown below:
' <Assembly: AssemblyVersion("1.0.*")> 

<Assembly: AssemblyVersion("12.08.*")> 
