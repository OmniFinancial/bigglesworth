﻿'------------------------------------------------------------------------------
' <auto-generated>
'     This code was generated by a tool.
'     Runtime Version:4.0.30319.276
'
'     Changes to this file may cause incorrect behavior and will be lost if
'     the code is regenerated.
' </auto-generated>
'------------------------------------------------------------------------------

Option Strict On
Option Explicit On

Imports System

Namespace My.Resources
    
    'This class was auto-generated by the StronglyTypedResourceBuilder
    'class via a tool like ResGen or Visual Studio.
    'To add or remove a member, edit your .ResX file then rerun ResGen
    'with the /str option, or rebuild your VS project.
    '''<summary>
    '''  A strongly-typed resource class, for looking up localized strings, etc.
    '''</summary>
    <Global.System.CodeDom.Compiler.GeneratedCodeAttribute("System.Resources.Tools.StronglyTypedResourceBuilder", "4.0.0.0"),  _
     Global.System.Diagnostics.DebuggerNonUserCodeAttribute(),  _
     Global.System.Runtime.CompilerServices.CompilerGeneratedAttribute(),  _
     Global.Microsoft.VisualBasic.HideModuleNameAttribute()>  _
    Friend Module Resources
        
        Private resourceMan As Global.System.Resources.ResourceManager
        
        Private resourceCulture As Global.System.Globalization.CultureInfo
        
        '''<summary>
        '''  Returns the cached ResourceManager instance used by this class.
        '''</summary>
        <Global.System.ComponentModel.EditorBrowsableAttribute(Global.System.ComponentModel.EditorBrowsableState.Advanced)>  _
        Friend ReadOnly Property ResourceManager() As Global.System.Resources.ResourceManager
            Get
                If Object.ReferenceEquals(resourceMan, Nothing) Then
                    Dim temp As Global.System.Resources.ResourceManager = New Global.System.Resources.ResourceManager("Bigglesworth.Resources", GetType(Resources).Assembly)
                    resourceMan = temp
                End If
                Return resourceMan
            End Get
        End Property
        
        '''<summary>
        '''  Overrides the current thread's CurrentUICulture property for all
        '''  resource lookups using this strongly typed resource class.
        '''</summary>
        <Global.System.ComponentModel.EditorBrowsableAttribute(Global.System.ComponentModel.EditorBrowsableState.Advanced)>  _
        Friend Property Culture() As Global.System.Globalization.CultureInfo
            Get
                Return resourceCulture
            End Get
            Set
                resourceCulture = value
            End Set
        End Property
        
        '''<summary>
        '''  Looks up a localized string similar to The encoded key is not in the proper DER format..
        '''</summary>
        Friend ReadOnly Property CryptHelper_BadDerFormat() As String
            Get
                Return ResourceManager.GetString("CryptHelper_BadDerFormat", resourceCulture)
            End Get
        End Property
        
        '''<summary>
        '''  Looks up a localized string similar to The encoded key is not in the proper PEM format..
        '''</summary>
        Friend ReadOnly Property CryptHelper_BadPemFormat() As String
            Get
                Return ResourceManager.GetString("CryptHelper_BadPemFormat", resourceCulture)
            End Get
        End Property
        
        '''<summary>
        '''  Looks up a localized string similar to Signability checked..
        '''</summary>
        Friend ReadOnly Property dkimDefaultSigner_Log_CanSign() As String
            Get
                Return ResourceManager.GetString("dkimDefaultSigner_Log_CanSign", resourceCulture)
            End Get
        End Property
        
        '''<summary>
        '''  Looks up a localized string similar to Got body hash..
        '''</summary>
        Friend ReadOnly Property dkimDefaultSigner_Log_GetBodyHash() As String
            Get
                Return ResourceManager.GetString("dkimDefaultSigner_Log_GetBodyHash", resourceCulture)
            End Get
        End Property
        
        '''<summary>
        '''  Looks up a localized string similar to Got canonicalized headers..
        '''</summary>
        Friend ReadOnly Property dkimDefaultSigner_Log_GetCanonicalizedHeaders() As String
            Get
                Return ResourceManager.GetString("dkimDefaultSigner_Log_GetCanonicalizedHeaders", resourceCulture)
            End Get
        End Property
        
        '''<summary>
        '''  Looks up a localized string similar to Got signed DKIM header..
        '''</summary>
        Friend ReadOnly Property dkimDefaultSigner_Log_GetSignedDKIMHeader() As String
            Get
                Return ResourceManager.GetString("dkimDefaultSigner_Log_GetSignedDKIMHeader", resourceCulture)
            End Get
        End Property
        
        '''<summary>
        '''  Looks up a localized string similar to Got unsigned DKIM header..
        '''</summary>
        Friend ReadOnly Property dkimDefaultSigner_Log_GetUnsignedDKIMHeader() As String
            Get
                Return ResourceManager.GetString("dkimDefaultSigner_Log_GetUnsignedDKIMHeader", resourceCulture)
            End Get
        End Property
        
        '''<summary>
        '''  Looks up a localized string similar to Created new Default Signer..
        '''</summary>
        Friend ReadOnly Property dkimDefaultSigner_Log_New() As String
            Get
                Return ResourceManager.GetString("dkimDefaultSigner_Log_New", resourceCulture)
            End Get
        End Property
        
        '''<summary>
        '''  Looks up a localized string similar to Signed..
        '''</summary>
        Friend ReadOnly Property dkimDefaultSigner_Log_Sign() As String
            Get
                Return ResourceManager.GetString("dkimDefaultSigner_Log_Sign", resourceCulture)
            End Get
        End Property
        
        '''<summary>
        '''  Looks up a localized string similar to Wrote signed MIME message..
        '''</summary>
        Friend ReadOnly Property dkimDefaultSigner_Log_WriteSignedMIMEMessage() As String
            Get
                Return ResourceManager.GetString("dkimDefaultSigner_Log_WriteSignedMIMEMessage", resourceCulture)
            End Get
        End Property
        
        '''<summary>
        '''  Looks up a localized string similar to The MIME body could not be located, so DKIM signing was skipped..
        '''</summary>
        Friend ReadOnly Property dkimSigningRoutingAgent_MimeBodyNotFound() As String
            Get
                Return ResourceManager.GetString("dkimSigningRoutingAgent_MimeBodyNotFound", resourceCulture)
            End Get
        End Property
        
        '''<summary>
        '''  Looks up a localized string similar to The PEM-encoded RSA Private Key is not specified, so DKIM signing was skipped..
        '''</summary>
        Friend ReadOnly Property dkimSigningRoutingAgent_NoKey() As String
            Get
                Return ResourceManager.GetString("dkimSigningRoutingAgent_NoKey", resourceCulture)
            End Get
        End Property
        
        '''<summary>
        '''  Looks up a localized string similar to Signing a mail item according to DKIM failed with an exception. Check the logged exception for details..
        '''</summary>
        Friend ReadOnly Property dkimSigningRoutingAgent_SignFailed() As String
            Get
                Return ResourceManager.GetString("dkimSigningRoutingAgent_SignFailed", resourceCulture)
            End Get
        End Property
        
        '''<summary>
        '''  Looks up a localized string similar to The algorithm specified in the configuration file could not be parsed. Check the value of the &apos;DefaultDkimSigner_Algorithm&apos; key under &lt;appSettings&gt;. Currently accepted values are &apos;RsaSha1&apos; (recommended) and &apos;RsaSha256&apos;..
        '''</summary>
        Friend ReadOnly Property dkimSigningRoutingAgentFactory_BadAlgorithmConfig() As String
            Get
                Return ResourceManager.GetString("dkimSigningRoutingAgentFactory_BadAlgorithmConfig", resourceCulture)
            End Get
        End Property
        
        '''<summary>
        '''  Looks up a localized string similar to The domain was not found in the configuration file or is blank. Check the value of the &apos;DefaultDkimSigner_Domain&apos; key under the &lt;appSettings&gt; section..
        '''</summary>
        Friend ReadOnly Property dkimSigningRoutingAgentFactory_BadDomainConfig() As String
            Get
                Return ResourceManager.GetString("dkimSigningRoutingAgentFactory_BadDomainConfig", resourceCulture)
            End Get
        End Property
        
        '''<summary>
        '''  Looks up a localized string similar to The PEM-encoded private RSA key was not found in the configuration file or is blank. Check the value of the &apos;DefaultDkimSigner_PrivateKey&apos; key in the &lt;appSettings&gt; section..
        '''</summary>
        Friend ReadOnly Property dkimSigningRoutingAgentFactory_BadPrivateKeyConfig() As String
            Get
                Return ResourceManager.GetString("dkimSigningRoutingAgentFactory_BadPrivateKeyConfig", resourceCulture)
            End Get
        End Property
        
        '''<summary>
        '''  Looks up a localized string similar to The selector was not found in the configuration file or is blank. Check the value of the &apos;DefaultDkimSigner_Selector&apos; key under &lt;appSettings&gt;..
        '''</summary>
        Friend ReadOnly Property dkimSigningRoutingAgentFactory_BadSelectorConfig() As String
            Get
                Return ResourceManager.GetString("dkimSigningRoutingAgentFactory_BadSelectorConfig", resourceCulture)
            End Get
        End Property
        
        '''<summary>
        '''  Looks up a localized string similar to Creating new instance of dkimSigningRoutingAgentFactory..
        '''</summary>
        Friend ReadOnly Property dkimSigningRoutingAgentFactory_Log_Create() As String
            Get
                Return ResourceManager.GetString("dkimSigningRoutingAgentFactory_Log_Create", resourceCulture)
            End Get
        End Property
        
        '''<summary>
        '''  Looks up a localized string similar to Creating new instance of DkimSigningRoutingAgent..
        '''</summary>
        Friend ReadOnly Property dkimSigningRoutingAgentFactory_Log_CreateAgent() As String
            Get
                Return ResourceManager.GetString("dkimSigningRoutingAgentFactory_Log_CreateAgent", resourceCulture)
            End Get
        End Property
        
        '''<summary>
        '''  Looks up a localized string similar to There was an error initializing the signing routing agent factory..
        '''</summary>
        Friend ReadOnly Property dkimSigningRoutingAgentFactory_Log_InitializationError() As String
            Get
                Return ResourceManager.GetString("dkimSigningRoutingAgentFactory_Log_InitializationError", resourceCulture)
            End Get
        End Property
    End Module
End Namespace
