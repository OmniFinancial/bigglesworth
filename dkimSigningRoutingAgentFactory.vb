﻿
' This file is part of Bigglesworth.
'
' Bigglesworth is free software: you can redistribute it and/or modify
' it under the terms of the GNU General Public License as published by
' the Free Software Foundation, either version 3 of the License, or
' (at your option) any later version.
'
' Bigglesworth is distributed in the hope that it will be useful,
' but WITHOUT ANY WARRANTY; without even the implied warranty of
' MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
' GNU General Public License for more details.
'
' You should have received a copy of the GNU General Public License
' along with Bigglesworth.  If not, see <http://www.gnu.org/licenses/>.

Imports System.Collections.Generic, System.Configuration, System.IO, System.Reflection ', log4net, log4net.Config

''' <summary>
''' Creates new instances of the dkimSigningRoutingAgent.
''' </summary>
''' <remarks></remarks>
Public NotInheritable Class dkimSigningRoutingAgentFactory
    Inherits RoutingAgentFactory

#Region " Shared Fields / Properties "

#End Region
    ''' <summary>
    ''' Instance of logger for this class.
    ''' </summary>
    ''' <remarks></remarks>
    'Private Shared _log As ILog = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType)

#Region " Instance Fields / Properties "
    ''' <summary>
    ''' The algorithm that should be used in signing.
    ''' </summary>
    Private _algorithm As dkimAlgorithmKind
    ''' <summary>
    ''' The domain that is used when signing.
    ''' </summary>
    Private _domain As String
    ''' <summary>
    ''' The PEM-encoded private key.
    ''' </summary>
    Private _encodedKey As String
    ''' <summary>
    ''' The headers to sign in each message.
    ''' </summary>
    Private _headersToSign As IEnumerable(Of String)
    ''' <summary>
    ''' The selector to use when signing a message.
    ''' </summary>
    Private _selector As String
#End Region

#Region " Instance Methods "
    ''' <summary>
    ''' When overriden in a derived class, the 
    ''' <see cref="M:Microsoft.Exchange.Data.Transport.Routing.RoutingAgentFactory.CreateAgent(Microsoft.Exchange.Data.Transport.SmtpServer)"/> 
    ''' method returns an instance of a routing agent.
    ''' </summary>
    ''' <param name="server">The server on which the routing agent will operate.</param>
    ''' <returns>The <see cref="DkimSigningRoutingAgent"/> instance.</returns>
    ''' <remarks></remarks>
    Public Overrides Function CreateAgent(server As Microsoft.Exchange.Data.Transport.SmtpServer) As Microsoft.Exchange.Data.Transport.Routing.RoutingAgent
        If Logger.SetVerbosity >= Logger.Verbosity.Medium Then
            Dim lmessage As String = My.Resources.dkimSigningRoutingAgentFactory_Log_CreateAgent
            'Debug.WriteLine(lmessage)
            'My.Application.Log.WriteEntry(lmessage)
            Logger.Log(Logger.Verbosity.Medium, lmessage)
        End If
        Return New dkimSigningRoutingAgent(New dkimDefaultSigner(_algorithm, _headersToSign, _selector, _domain, _encodedKey))
    End Function
    Public Overloads Function CreateAgent() As Microsoft.Exchange.Data.Transport.Routing.RoutingAgent
        If Logger.SetVerbosity >= Logger.Verbosity.Medium Then
            Dim lmessage As String = My.Resources.dkimSigningRoutingAgentFactory_Log_CreateAgent
            'Debug.WriteLine(lmessage)
            'My.Application.Log.WriteEntry(lmessage)
            Logger.Log(Logger.Verbosity.Medium, lmessage)
        End If
        Return New dkimSigningRoutingAgent(New dkimDefaultSigner(_algorithm, _headersToSign, _selector, _domain, _encodedKey))
    End Function
    ''' <summary>
    ''' Initializes various settings based on configuration.
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub Initialize()
        Try
            ' Load configuration from file.
            ConfigHandler.Reload()
            ' Initialize the Logger.
            Logger.Initialize(My.Settings.LoggerVerbosity)
            ' Initialize Log4Net.
            'XmlConfigurator.Configure(New FileInfo(My.Settings.LogPath))
            ' Load the Signing algorithm.
            Try
                _algorithm = [Enum].Parse(GetType(dkimAlgorithmKind), My.Settings.dkimDefaultSigner_Algorithm, True)
            Catch ex As Exception
                Throw New ConfigurationErrorsException(My.Resources.dkimSigningRoutingAgentFactory_BadAlgorithmConfig, ex)
            End Try
            ' Load the list of headers to sign in each message.
            Dim lunparsedHeaders = My.Settings.dkimDefaultSigner_HeadersToSign
            If lunparsedHeaders IsNot Nothing Then _headersToSign = lunparsedHeaders.Split(New Char() {";"}, StringSplitOptions.RemoveEmptyEntries)
            ' Load the selector that is used when signing messages.
            _selector = My.Settings.dkimDefaultSigner_Selector
            If _selector Is Nothing OrElse _selector.Length = 0 Then Throw New ConfigurationErrorsException(My.Resources.dkimSigningRoutingAgentFactory_BadSelectorConfig)
            ' Load the domain that is used when signing messages.
            _domain = My.Settings.dkimDefaultSigner_Domain
            If _domain Is Nothing OrElse _domain.Length = 0 Then Throw New ConfigurationErrorsException(My.Resources.dkimSigningRoutingAgentFactory_BadDomainConfig)
            ' Load the PEM-encoded RSA private key.
            _encodedKey = My.Settings.dkimDefaultSigner_PrivateKey
            If _encodedKey Is Nothing OrElse _encodedKey.Length = 0 Then Throw New ConfigurationErrorsException(My.Resources.dkimSigningRoutingAgentFactory_BadPrivateKeyConfig)
        Catch ex As ConfigurationErrorsException
            'Debug.WriteLine(ex.Message)
            'My.Application.Log.WriteException(ex, TraceEventType.Error, ex.Message)
            '_log.Error(My.Resources.dkimSigningRoutingAgentFactory_Log_InitializationError)
            Logger.Log(Logger.Verbosity.Low, ex.Message)
            Throw ex
        End Try

        'Dim lconfig = ConfigurationManager.OpenExeConfiguration(Me.GetType.Assembly.Location)
        'Dim lappSettings = lconfig.AppSettings.Settings
        '' Load the signing algorithm.
        'Try
        '_algorithm = [Enum].Parse(GetType(dkimAlgorithmKind), lappSettings("dkimDefaultSigner_Algorithm").Value, True)
        'Catch ex As Exception
        '    Throw New ConfigurationErrorsException(My.Resources.dkimSigningRoutingAgentFactory_BadAlgorithmConfig, ex)
        'End Try
        '' Load the list of headers to sign in each message.
        'Dim lunparsedHeaders = lappSettings("dkimDefaultSigner_HeadersToSign").Value
        'If lunparsedHeaders IsNot Nothing Then _headersToSign = lunparsedHeaders.Split(New Char() {";"}, StringSplitOptions.RemoveEmptyEntries)
        '' Load the selector that is used when signing messages.
        '_selector = lappSettings("dkimDefaultSigner_Selector").Value
        'If _selector Is Nothing OrElse _selector.Length = 0 Then Throw New ConfigurationErrorsException(My.Resources.dkimSigningRoutingAgentFactory_BadSelectorConfig)
        '' Load the domain that is used when signing messages.
        '_domain = lappSettings("dkimDefaultSigner_Domain").Value
        'If _domain Is Nothing OrElse _domain.Length = 0 Then Throw New ConfigurationErrorsException(My.Resources.dkimSigningRoutingAgentFactory_BadDomainConfig)
        '' Load the PEM-encoded RSA private key.
        '_encodedKey = lappSettings("dkimDefaultSigner_PrivateKey").Value
        'If _encodedKey Is Nothing OrElse _encodedKey.Length = 0 Then Throw New ConfigurationErrorsException(My.Resources.dkimSigningRoutingAgentFactory_BadPrivateKeyConfig)
    End Sub
#End Region

#Region " Constructors "
    Public Sub New()
        Initialize()
        If Logger.SetVerbosity >= Logger.Verbosity.Low Then
            Dim lmessage As String = My.Resources.dkimSigningRoutingAgentFactory_Log_Create
            'Debug.WriteLine(lmessage)
            'My.Application.Log.WriteEntry(lmessage)
            '_log.Debug(lmessage)
            Logger.Log(Logger.Verbosity.Low, lmessage)
        End If
    End Sub
#End Region

End Class
